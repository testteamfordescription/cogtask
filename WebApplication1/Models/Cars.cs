﻿using System.Collections.Generic;

namespace Models
{
    public class Cars
    {
        public string location { get; set; }
        public List<Vehicle> vehicles { get; set; }
    }
}
