﻿using DBRepository.Interfaces;

namespace DBRepository
{
    public class DataBaseSettings : IDataBasesettings
    {
        public string WarehouseCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
