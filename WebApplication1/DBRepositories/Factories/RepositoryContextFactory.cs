﻿using DBRepository.Interfaces;
using MongoDB.Driver;

namespace DBRepository.Factories
{
    public class RepositoryContextFactory : IRepositoryContextFactory
    {
        private readonly IDataBasesettings _settings;
        public RepositoryContextFactory(IDataBasesettings settings)
        {
            _settings = settings;
        }

        public IMongoDatabase CreateDbContext()
        {
            var client = new MongoClient(_settings.ConnectionString);
            return client.GetDatabase(_settings.DatabaseName);
        }
    }
}
