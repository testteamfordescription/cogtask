﻿using MongoDB.Driver;

namespace DBRepository.Interfaces
{
    public interface IRepositoryContextFactory
    {
        IMongoDatabase CreateDbContext();
    }
}
