﻿using Models;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace DBRepository.Interfaces
{
    public interface ICarRepository
    {
        Task<Vehicle> GetVehicleByIdAndWarehouseIdAsync(int vehicleId, string warehouseId);
        Task<Cars> GetAllCarsByWarehouseIdAsync(string id);
        IMongoCollection<Warehouse> GetAllWarehouses();
    }
}
