﻿namespace DBRepository.Interfaces
{
    public interface IDataBasesettings
    {
        string WarehouseCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
