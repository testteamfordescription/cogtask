﻿using DBRepository.Interfaces;

namespace DBRepository.Repositories
{
    public abstract class BaseRepository
    {
        protected IRepositoryContextFactory ContextFactory { get; }
        protected IDataBasesettings Settings { get; }

        protected BaseRepository(IRepositoryContextFactory contextFactory, IDataBasesettings settings)
        {
            ContextFactory = contextFactory;
            Settings = settings;
        }
    }
}
