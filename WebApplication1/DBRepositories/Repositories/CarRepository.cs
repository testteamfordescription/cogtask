﻿using System.Threading.Tasks;
using DBRepository.Interfaces;
using Models;
using MongoDB.Driver;

namespace DBRepository.Repositories
{
    public class CarRepository : BaseRepository, ICarRepository
    {
        private readonly IMongoDatabase _context;
        private readonly IDataBasesettings _settings;

        public CarRepository(IRepositoryContextFactory contextFactory, IDataBasesettings settings) : base(contextFactory, settings)
        {
            _context = contextFactory.CreateDbContext();
            _settings = settings;
        }

        public async Task<Cars> GetAllCarsByWarehouseIdAsync(string id)
        {
            Warehouse warehouse = await GetWarehouseByIdAsync(id);
            return warehouse.cars;
        }

        public IMongoCollection<Warehouse> GetAllWarehouses()
        {
            return _context.GetCollection<Warehouse>(_settings.WarehouseCollectionName);
        }

        public async Task<Vehicle> GetVehicleByIdAndWarehouseIdAsync(int vehicleId, string warehouseId)
        {
            Warehouse warehouse = await GetWarehouseByIdAsync(warehouseId);
            return warehouse.cars.vehicles.Find(c => c._id == vehicleId);
        }

        private async Task<Warehouse> GetWarehouseByIdAsync(string id)
        {
            return await GetAllWarehouses().Find(w => w._id == id).FirstOrDefaultAsync();
        }
    }
}
