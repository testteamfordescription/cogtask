﻿using System.Collections.Generic;
using DBRepository.Interfaces;
using Models;
using MongoDB.Driver;
using System.Threading.Tasks;
using WebApplication1.Interfaces;

namespace WebApplication1.Services
{
    public class CarService : ICarService
    {
        private readonly ICarRepository _carRepository;

        public CarService(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }

        public async Task<Cars> GetAllCarsByWarehouseIdAsync(string id)
        {
            return await _carRepository.GetAllCarsByWarehouseIdAsync(id);
        }

        public async Task<List<Warehouse>> GetAllWarehouses()
        {
            return await _carRepository.GetAllWarehouses().AsQueryable().ToListAsync();
        }

        public async Task<Vehicle> GetVehicleByIdAndWarehouseIdAsync(int vehicleId, string warehouseId)
        {
            return await _carRepository.GetVehicleByIdAndWarehouseIdAsync(vehicleId, warehouseId);
        }

        
    }
}
