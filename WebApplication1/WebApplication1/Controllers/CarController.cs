﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Models;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication1.Interfaces;
using WebApplication1.Services;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CarController : ControllerBase
    {
        private readonly ILogger<CarController> _logger;
        private readonly ICarService _carService;
        public CarController(ILogger<CarController> logger, ICarService carService)
        {
            _logger = logger;
            _carService = carService;
        }

        [HttpGet]
        [Route("GetVehicleById")]
        public async Task<ActionResult<Vehicle>> GetVehicleById(int vehicleId, string warehouseId)
        {
            return Ok(await _carService.GetVehicleByIdAndWarehouseIdAsync(vehicleId, warehouseId));
        }

        [HttpGet]
        [Route("GetAllCarsByWarehouseId")]
        public async Task<ActionResult<IEnumerable<Cars>>> GetAllCarsByWarehouseId(string warehouseId)
        {
            return Ok(await _carService.GetAllCarsByWarehouseIdAsync(warehouseId));
        }

        [HttpGet]
        [Route("GetAllWarehouses")]
        public async Task<ActionResult<IEnumerable<Warehouse>>> GetAllWarehouses()
        {
            return Ok(await _carService.GetAllWarehouses());
        }
    }
}
