﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Models;

namespace WebApplication1.Interfaces
{
    public interface ICarService
    {
        Task<Vehicle> GetVehicleByIdAndWarehouseIdAsync(int vehicleId, string warehouseId);
        Task<Cars> GetAllCarsByWarehouseIdAsync(string id);
        Task<List<Warehouse>> GetAllWarehouses();
    }
}
