import './App.css';
import CarListComponent from './components/carListComponent'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <CarListComponent/>
      </header>
    </div>
  );
}

export default App;
