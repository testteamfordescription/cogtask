import React from 'react'

export default class CarComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                {this.props.make} {this.props.model}
            </div>
        );
    }
}